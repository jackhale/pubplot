# pubplot #

pubplot is designed to make it easy to produce publication quality plots using matplotlib.

![plot.png](https://bitbucket.org/repo/8ggeak/images/2241744134-plot.png)

The default matplotlib plots are fine for screen viewing but do not look great in journals. This package sets matplotlib up in a journal friendly format with 3.75in single-column wide images, height proportioned according to the golden ratio, LaTeX typeset labels, appropriate but narrow whitespace around the image, 9 and 10pt fonts, and output at 1000dpi if you choose to export as TIFF or PNG (PDF is recommended).

## Installation ##

matplotlib and a functioning TeXLive install are required.

```
python setup.py
```

or 

```
pip install git+ssh://git@bitbucket.org:jackhale/pubplot.git
```

## Usage ##

```
from matplotlib import pyplot as plt
import pubplot

pubplot.setup()

ax = pubplot.generate()
# Perform plotting with ax object
plt.savefig('output.pdf')
plt.show()
```

## License ##

pubplot is released into the public domain.