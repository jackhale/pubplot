from mpl_toolkits.mplot3d import Axes3D
import pylab
import numpy as np

# Publication plot settings
def setup(width=90/25.4):
	golden_mean = (np.sqrt(5)-1.0)/2.0
	fig_width = width
	fig_height = width*golden_mean
	fig_size =  [fig_width,fig_height]
	params = {'axes.labelsize': 10,
              'font.size': 10,
              'legend.fontsize': 9,
              'axes.titlesize': 10,
              'xtick.labelsize': 10,
              'ytick.labelsize': 10,
              'lines.markersize': 5,
	          'font.size': 10,
	          'text.usetex': True,
              'figure.figsize': fig_size,
              'figure.dpi': 1000,
              'font.family': 'serif',
	          'font.serif': 'Palatino',
              'legend.handlelength': 3.0,
	          'font.sans-serif': 'Helvetica'}
	pylab.rcParams.update(params)

def generate(title=False, projection='rectilinear'):
    fig = pylab.figure()
    pylab.clf()
    if projection == '3d':
        ax = pylab.axes(projection=projection)
    elif title == False and projection == 'rectilinear':
        ax = pylab.axes([0.17,0.17,0.95-0.17,0.95-0.17],projection=projection)
    elif title == True and projection == 'rectilinear':
        ax = pylab.axes([0.25,0.25,0.95-0.25,0.95-0.25],projection=projection)
    return ax
